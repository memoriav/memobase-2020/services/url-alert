# Url Alert

The alerting component of the new "sitemap" infrastructure. It is 
responsible for querying the database for erroneous urls and alert the 
respective institution via e-mail

## Configuration

In order to work as expected, the service needs to have a couple of environment variables set:

* `MARIADB_HOST`: Host name of the MariaDB server
* `MARIADB_PORT`: Port of the MariaDB server
* `MARIADB_USER`: User of the MariaDB server
* `MARIADB_PASSWORD`: Password of the MariaDB server
* `MARIADB_DATABASE`: Respective database
* `MARIADB_TABLE`: Table where the errors are stored
* `MARIADB_HOST_CERTS`: Path to MariaDB root certificates
* `SMTP_HOST`: Host name of the SMTP server
* `SMTP_PORT`: Port of the SMTP server
* `SMTP_USER`: User which is used to send e-mails
* `SMTP_PASSWORD`: Respective password
* `ADMIN_MAIL`: E-mail address of the administrator (is informed about each error)
* `INTERNAL_INSTITUTION_MAIL`: E-mail address where errors concering locally stored resources are sent to
* `INSTITUTION_EMAILS_PATH`: Path to the file containing the signature-institution e-mail address mapping (see below)
* `ERROR_GRACE_PERIOD`: Minimal age (in hours) of the error before propagated to the respective institution
* `RENOTIFY_AFTER`: Duration (in hours) after which the institution is informed again on the error if it still persists

### Signature-institution mapping

The service expects a comma-separated mapping between signature of the collection and one or more e-mail addresses of the responsible institution(s). Use linebreaks for separating collections:

```sh
<sig>,<e-mail 1>,<e-mail 2>
```

For instance:
```text
afp-001,afp@example.com,afp2@example.com
xyz-002,info@nowhere.ch
```

The file must be available on the designated path (`INSTITUTION_EMAILS_PATH`)
