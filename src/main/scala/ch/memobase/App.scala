/*
 * Url Alert
 * Copyright (C) 2021  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase

import ch.memobase.models.{Institution, Records}
import jakarta.mail.internet.MimeMessage
import org.apache.logging.log4j.scala.Logging

import scala.util.{Failure, Success, Try}

object App
    extends scala.App
    with Logging
    with AppSettings
    with MariaDBClient
    with MailClient
    with RecipientsHandler {

  private val logSendMail: MimeMessage => Unit = mail =>
    sendMail(mail) match {
      case Success(_) =>
        logger.info(
          s"Mail successfully sent to ${mail.getAllRecipients.mkString(", ")}"
        )
      case Failure(ex) =>
        logger.warn(
          s"Mail couldn't be sent to ${mail.getAllRecipients.mkString(", ")}: ${ex.getMessage}"
        )
    }

  private def processRecords(
      records: Try[Records],
      firstTime: Boolean
  ): Try[Unit] = {
    records
      .flatMap(records =>
        Try(
          records
            .getRecordsByInstitutionAndCollections(internalInstitution)
            .flatMap { institution =>
              {
                logger.debug(s"Processing institution ${institution._1}")
                Seq(
                  Institution(
                    institution._1,
                    getRecipient(institution._1),
                    institution._2,
                    institution._1 == internalInstitution
                  )
                )
              }
            }
            .flatMap { institution =>
              if (institution.emails.isEmpty) {
                logger.info(
                  s"Institution ${institution.id} has no email addresses. Skipping..."
                )
                Seq()
              } else if (institution.collections.isEmpty) {
                logger.info("No collections found. Skipping")
                Seq()
              } else {
                createMail(
                  institution.emails,
                  institution.collections,
                  createdBefore,
                  firstTime = firstTime,
                  internal = institution.internal
                ) match {
                  case Success(mail) => Seq(mail)
                  case Failure(ex) =>
                    logger.warn(
                      s"Couldn't create message for recipients ${institution.emails
                        .mkString(", ")}: ${ex.getMessage}"
                    )
                    Seq()
                }
              }
            }
            .foreach {
              logSendMail
            }
        )
      )
  }

  private def updateNotifiedonField(records: Try[Records]): Try[Unit] = {
    records
      .flatMap(records => Try(records.getIds))
      .flatMap(ids => Try(updateTimestamp(ids)))
  }

  logger.info("Fetching first time errors from DB")
  val firstTimeErrors = fetchFirstTimeErrors
  processRecords(fetchFirstTimeErrors, firstTime = true)
    .recover { case e =>
      logger.error(s"Fetching first time errors failed: ${e.getMessage}")
      sys.exit(1)
    }
  updateNotifiedonField(firstTimeErrors)
    .recover { case e =>
      logger.error(
        s"Updating `notifiedon` field for first time errors failed: ${e.getMessage}"
      )
      sys.exit(1)
    }
  logger.info(
    s"Processing first time errors finished: ${firstTimeErrors.get.size} records found"
  )

  logger.info("Fetching persisting errors from DB")
  val persistingErrors = fetchPersistingErrors
  processRecords(persistingErrors, firstTime = false)
    .recover { case e =>
      logger.error(s"Fetching persisting errors failed: ${e.getMessage}")
      sys.exit(1)
    }
  updateNotifiedonField(persistingErrors)
    .recover { case e =>
      logger.error(
        s"Updating `notifiedon` field for persisting errors failed: ${e.getMessage}"
      )
      sys.exit(1)
    }
  logger.info(
    s"Processing persisting errors finished: ${persistingErrors.get.size} records found"
  )

  logger.info("Shutting down application")
}
