/*
 * Url Alert
 * Copyright (C) 2021  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase

import java.text.SimpleDateFormat
import java.time.Duration
import java.time.temporal.ChronoUnit
import java.util.{Calendar, Date, TimeZone}
import scala.jdk.CollectionConverters._
import scala.io.Source

trait AppSettings {

  val dateFormatter = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss")

  lazy val institutionEmails: Map[String, Seq[String]] = {
    Source
      .fromFile(sys.env("INSTITUTION_EMAILS_PATH"), "UTF-8")
      .bufferedReader()
      .lines()
      .iterator()
      .asScala
      .map(_.split(",").toList)
      .collect { case sig :: emails =>
        sig -> emails
      }
      .toMap
  }

  val smtpHost: String = sys.env("SMTP_HOST").replaceAll("\\s+$", "")
  val smtpPort: String = sys.env("SMTP_PORT").replaceAll("\\s+$", "")
  val smtpUser: String = sys.env("SMTP_USER").replaceAll("\\s+$", "")
  val smtpPwd: String = sys.env("SMTP_PASSWORD").replaceAll("\\s+$", "")
  val adminMail: String = sys.env("ADMIN_MAIL")
  val internalInstitutionMail: String = sys.env("INTERNAL_INSTITUTION_MAIL")

  val mariaDBDSN: String =
    s"jdbc:mariadb://${sys.env("MARIADB_HOST")}:${sys.env("MARIADB_PORT")}/${sys
      .env("MARIADB_DATABASE")}?sslMode=verify-full&serverSslCert=${sys.env("MARIADB_HOST_CERTS")}"
  val mariaDBTable: String = sys.env("MARIADB_TABLE")
  val mariaDBUser: String = sys.env("MARIADB_USER").replaceAll("\\s+$", "")
  val mariaDBPwd: String = sys.env("MARIADB_PASSWORD").replaceAll("\\s+$", "")
  val dbConnectionTimeout: Int = 10

  val gracePeriod: Duration =
    Duration.of(sys.env("ERROR_GRACE_PERIOD").toLong, ChronoUnit.HOURS)
  val renotifyAfter: Duration =
    Duration.of(sys.env("RENOTIFY_AFTER").toLong, ChronoUnit.HOURS)

  val internalInstitution: String = "_internal"

  private val now =
    Calendar.getInstance(TimeZone.getTimeZone("Europe/Zurich"))

  val nowAsString: String = dateFormatter.format(now.getTime)

  private val before: Long => Date = delta => {
    val before = now.getTimeInMillis - (delta * 1000)
    val date = Calendar.getInstance()
    date.setTimeInMillis(before)
    date.getTime
  }

  /** Determines the time before which records with defective URLs have to be
    * created.
    */
  val createdBefore: Date = before(gracePeriod.get(ChronoUnit.SECONDS))

  val notifiedBefore: Date = before(renotifyAfter.get(ChronoUnit.SECONDS))

}
