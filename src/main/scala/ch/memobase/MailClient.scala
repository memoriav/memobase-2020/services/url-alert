/*
 * Url Alert
 * Copyright (C) 2021  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase

import ch.memobase.models.Record
import com.github.tototoshi.csv.CSVWriter
import jakarta.activation.{DataHandler, FileDataSource}
import jakarta.mail._
import jakarta.mail.internet.{MimeBodyPart, MimeMessage, MimeMultipart}
import org.apache.logging.log4j.scala.Logging

import java.io.File
import java.text.SimpleDateFormat
import java.util.{Calendar, Date, Locale, Properties}
import scala.util.Try

trait MailClient {

  self: Logging & AppSettings =>

  private val outputFormat =
    new SimpleDateFormat("EEEE, d. MMMM", new Locale("de", "CH"))
  private val firstTimeErrorMailText: Iterable[String] => String => String =
    collections =>
      date => {
        val collectionText = if (collections.size > 1) {
          s"""Ihren Beständen
         |- ${collections.mkString("\n- ")}
         |""".stripMargin
        } else {
          s"Ihrem Bestand ${collections.head}"
        }
        val listText = if (collections.size > 1) "Listen" else "Liste"
        s"""Guten Tag
       |
       |Wir haben festgestellt, dass eine Reihe von URLs in $collectionText
       |seit dem $date nicht mehr aufgelöst werden können.
       |Sie können die $listText der betroffenen Aufnahmen im Anhang ersehen.
       |
       |Freundliche Grüsse
       |Ihr Memobase-Team
       |""".stripMargin
      }

  private val persistingErrorMailText: Iterable[String] => String => String =
    collections =>
      date => {
        val collectionText = if (collections.size > 1) {
          s"""Ihren Beständen
         |- ${collections.mkString("\n- ")}
         |""".stripMargin
        } else {
          s"Ihrem Bestand ${collections.head}"
        }
        val listText = if (collections.size > 1) "Listen" else "Liste"
        s"""Guten Tag
       |
       |Gerne möchten wir Sie darauf aufmerksam machen, dass die URLs
       |in $collectionText noch immer nicht aufgelöst werden können. Sie
       |können die $listText der betroffenen Aufnahmen im Anhang ersehen.
       |
       |Freundliche Grüsse
       |Ihr Memobase-Team
       |""".stripMargin
      }

  private val session: Session = {
    val properties = new Properties()
    properties.put("mail.transport.protocol", "smtp")
    properties.put("mail.smtp.starttls.enable", "true")
    properties.put("mail.smtp.host", smtpHost)
    properties.put("mail.smtp.port", smtpPort)
    properties.put("mail.smtp.auth", "true")
    val authenticator = new Authenticator() {
      override def getPasswordAuthentication =
        new PasswordAuthentication(smtpUser, smtpPwd)
    }
    Session.getDefaultInstance(properties, authenticator)
  }

  private def generateList(
      records: Seq[Record],
      collectionName: String
  ): String = {
    val f: File = new File(s"/tmp/$collectionName.csv")
    val csvWriter = CSVWriter.open(f, "UTF-8")
    csvWriter.writeAll(records.map(line => Seq(line.recordId, line.uri)))
    csvWriter.close
    f.getPath
  }

  def createMail(
      to: Seq[String],
      collections: Map[String, Seq[Record]],
      date: Date,
      firstTime: Boolean,
      internal: Boolean
  ): Try[MimeMessage] = Try {
    val formattedDate = outputFormat.format(date)
    logger.debug(s"Creating mail for ${to.mkString(", ")}")
    val message = new MimeMessage(session)
    message.setRecipients(Message.RecipientType.TO, to.mkString(","))
    message.setRecipients(Message.RecipientType.CC, adminMail)
    message.setFrom(smtpUser)
    message.setSubject(
      s"Memobase: Ungültige ${if (internal) "interne " else ""}URLs festgestellt"
    )
    message.setSentDate(Calendar.getInstance.getTime)

    val multipart = new MimeMultipart

    val textBodyPart = new MimeBodyPart
    textBodyPart.setText(
      if (firstTime) firstTimeErrorMailText(collections.keys)(formattedDate)
      else persistingErrorMailText(collections.keys)(formattedDate)
    )
    multipart.addBodyPart(textBodyPart)

    for (collection <- collections) {
      val listBodyPart = new MimeBodyPart
      val filename = generateList(collection._2, collection._1)
      val fileSource = new FileDataSource(filename)
      listBodyPart.setDataHandler(new DataHandler(fileSource))
      listBodyPart.setFileName(filename)
      multipart.addBodyPart(listBodyPart)
    }

    message.setContent(multipart)
    message
  }

  def sendMail(message: MimeMessage): Try[Unit] = Try {
    Transport.send(message)
  }
}
