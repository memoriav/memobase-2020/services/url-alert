/*
 * Url Alert
 * Copyright (C) 2021  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase

import ch.memobase.models.{Record, Records}
import org.apache.logging.log4j.scala.Logging

import java.sql.{Connection, DriverManager, ResultSet}
import scala.util.Try

trait MariaDBClient {
  self: Logging & AppSettings =>

  private var connection = getDBConnection

  private def getDBConnection: Connection = {
    val c = DriverManager.getConnection(mariaDBDSN, mariaDBUser, mariaDBPwd)
    c.setAutoCommit(false)
    c
  }

  private def checkDBConnection(timeoutSec: Int): Unit = {
    if (connection == null || !connection.isValid(timeoutSec)) {
      logger.info("DB connection is non-existent or invalid.")
      if (!connection.isClosed) {
        connection.close()
      }
      connection = getDBConnection
    }
  }

  private def extractRecord(resultSet: ResultSet): Try[Record] = Try {
    val sig = resultSet.getString("sig")
    val url = resultSet.getString("uri")
    Record(sig, url)
  }

  private def fetchUrls(sql: String): Try[Records] = Try {
    checkDBConnection(dbConnectionTimeout)
    val statement = connection.createStatement()
    logger.debug(sql)
    val resultSet = statement.executeQuery(sql)
    Iterator
      .continually(resultSet)
      .takeWhile(_.next)
      .map { rS =>
        {
          val record = extractRecord(rS)
          if (record.isFailure) {
            logger.warn(s"Extraction failed: ${record.failed.get.getMessage}")
          } else {
            logger.debug(s"Processing record ${record.get.recordId}")
          }
          record.toOption
        }
      }
      .collect {
        case r if r.isDefined => r.get
      }
      .foldLeft(new Records())((agg, rec) => agg.addRecord(rec))
  }

  def fetchFirstTimeErrors: Try[Records] =
    fetchUrls(
      s"SELECT sig, uri FROM $mariaDBTable WHERE status = 'error' and lastchange < '${dateFormatter
        .format(createdBefore)}' and notifiedon is NULL;"
    )

  def fetchPersistingErrors: Try[Records] =
    fetchUrls(
      s"SELECT sig, uri FROM $mariaDBTable WHERE status = 'error' and notifiedon < '${dateFormatter
        .format(notifiedBefore)}';"
    )

  def updateTimestamp(signatures: Seq[String]): Try[Array[Int]] = Try {
    checkDBConnection(dbConnectionTimeout)
    val statement = connection.createStatement
    val sql: String => String = signature =>
      s"UPDATE $mariaDBTable SET notifiedon = '$nowAsString' WHERE sig = '$signature';"
    for (signature <- signatures) {
      logger.debug(sql(signature))
      statement.addBatch(sql(signature))
    }
    val res = statement.executeBatch
    connection.commit()
    res
  }

  /** Closes connection to database
    */
  def closeDBConnection(): Try[Unit] = Try {
    logger.debug("Closing connection to database")
    connection.close()
  }
}
