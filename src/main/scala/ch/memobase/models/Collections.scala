/*
 * Url Alert
 * Copyright (C) 2021  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase.models

import scala.collection.mutable

class Collections {
  private val index = mutable.Map[String, Collection]()

  def addRecord(collectionId: String, record: Record): Collections = {
    if (index.contains(collectionId)) {
      val oldCollection = index(collectionId)
      val updatedCollection = oldCollection.copy(records = oldCollection.records :+ record)
      index.update(collectionId, updatedCollection)
    } else {
      index.put(collectionId, Collection(collectionId, records = Seq(record)))
    }
    this
  }

  def getCollection(collectionId: String): Option[Collection] = index.get(collectionId)

  def collections: Iterable[Collection] = index.values

}
