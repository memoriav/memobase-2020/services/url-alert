/*
 * Url Alert
 * Copyright (C) 2021  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase.models

import org.apache.logging.log4j.scala.Logging

import scala.collection.mutable

class Records extends Logging {
  type RecordIndex = Map[String, Map[String, Seq[Record]]]
  private val records = mutable.Buffer[Record]()

  def addRecord(rec: Record): Records = {
    logger.debug(s"Add record ${rec.recordId} to Records instance")
    records += rec
    this
  }

  def getRecordsByInstitutionAndCollections(internalMarker: String): RecordIndex = {
    logger.debug(s"Creating institution / recordSet index")
    val (internal, external) = records.
      partition(rec => rec.internal)
    val index = external.
      foldLeft(Map[String, Map[String, Seq[Record]]]())((index, rec) => {
        val institution = index.getOrElse(rec.institutionId, Map())
        val collection = institution.getOrElse(rec.collectionId, Seq())
        index ++ Map(rec.institutionId -> (institution ++ Map(rec.collectionId -> (collection :+ rec))))
      }) ++
      Map(internalMarker -> internal.foldLeft(Map[String, Seq[Record]]())((index, rec) => {
        val collection = index.getOrElse(rec.collectionId, Seq())
        index ++ Map(rec.collectionId -> (collection :+ rec))
      }))
    logger.debug("Institution / recordSet index created")
    index
  }

  def getIds: Seq[String] = records.map(r => r.recordId).toSeq

  def size: Int = records.size
}
