/*
 * Url Alert
 * Copyright (C) 2021  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase.models

import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.must.Matchers

class RecordsTest extends AnyFunSuite with Matchers {
  private val index = {
    val record1 = Record("abc-001-rec1", "https://example.com/rec1")
    val record2 = Record("def-001-rec2", "https://example.com/rec2")
    val record3 = Record("ghi-002-rec3", "https://example.com/rec3")
    val record4 = Record("abc-001-rec4", "https://example.com/rec4")
    val record5 = Record("jkl-001-rec5", "file://example.com/rec5")
    val record6 = Record("ghi-001-rec6", "https://example.com/rec6")
    val record7 = Record("mno-001-rec7", "file://example.com/rec7")
    val records = new Records()
    records
      .addRecord(record1)
      .addRecord(record2)
      .addRecord(record3)
      .addRecord(record4)
      .addRecord(record5)
      .addRecord(record6)
      .addRecord(record7)
    records.getRecordsByInstitutionAndCollections("_internal")
  }

  test("every institution should have a different index path (unless the records are internal)") {
    assert(index.keys.size == 4)
  }

  test("records with the same collection should be found on the same index path") {
    assert(index("abc")("abc-001").size == 2)
  }

  test("records in different collections of the same institution should indeed share a common institution parent") {
    assert(index("ghi")("ghi-001").size == 1 && index("ghi")("ghi-002").size == 1)
  }

  test("a record with a path to a local file should be declared as internal") {
    assert(index("_internal")("jkl-001").head.internal)
  }
}